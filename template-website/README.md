# template-website
Website made from scratch by me.

Images on the center are a simple image slider.

Live website:
https://template-website.neocities.org/

Screenshot:
![alt screenshot](https://s10.postimg.org/ukwpog689/website.png)
